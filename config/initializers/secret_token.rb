# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
DemoApp::Application.config.secret_key_base = 'b5af7fdd321bfc8634e7bb7e1424da63577b985c43bb8525dcc5a359bc302257b126b0ed364e0b4c98418e63a8df70d37511492f4aac87fc9686ee6b8d03a8fb'
